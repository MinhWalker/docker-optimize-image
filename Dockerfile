# FROM node:14-alpine

# WORKDIR /dir

# COPY . .

# RUN yarn install
# RUN yarn build
# CMD ["yarn", "start"]


# stage deps run yarn install to get full nodes_modules 
# has all packages in dependencies and devDependencies
FROM node:14-alpine AS deps             

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

# Rebuild the source code only when needed
# option --production keep package define on dependencies, ignore package on devDependencies
FROM node:14-alpine AS builder
WORKDIR /app
COPY . .
COPY --from=deps /app/node_modules ./node_modules
RUN yarn build && yarn install --production --ignore-scrips --prefer-offline

# Production image, copy all the files and run next
FROM node:14-alpine AS runner
WORKDIR /app

ENV NODE_ENV production

# Create user next js with UID:GID:1001:1001
RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

# Copy all thing that need for production from stage builder
COPY --from=builder /app/public ./public
COPY --from=builder --chown=nextjs:nodejs /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

USER nextjs

CMD ["yarn", "start"]